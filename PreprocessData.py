import xml.etree.ElementTree as ET
import pickle
import os
from os import listdir, getcwd
from os.path import join
import subprocess
import sys
from PIL import Image
from glob import glob


def getClasses(main_file):
    readIn = []
    try:
        classes_file = open("%s/classes.names" % main_file)
    except:
        assert(0), "Error : Please provide a file in the %s directory called classes.names." % main_file
    for line in classes_file:
        readIn.append(line.strip('\n'))
    return readIn


def setUp():
    if not os.path.exists('Training-Data'):
        os.makedirs('Training-Data')
    if not os.path.exists('Training-Data/image_data'):
        os.makedirs('Training-Data/image_data')
    if not os.path.exists('Training-Data/image_data/annotations'):
        os.makedirs('Training-Data/image_data/annotations')
    if not os.path.exists('Training-Data/image_data/images'):
        os.makedirs('Training-Data/image_data/images')
    if not os.path.exists('Training-Data/image_data/labels'):
        os.makedirs('Training-Data/image_data/labels')
    if not os.path.exists('Training-Data/image_data/unlabeled'):
        os.makedirs('Training-Data/image_data/unlabeled')


def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x, y, w, h)


def xml2txt(main_file, rawname, classes):
    subprocess.check_output(['cp', '%s/%s.xml' % (main_file, rawname), 'Training-Data/image_data/annotations'])
    in_file = open('%s/%s.xml' % (main_file, rawname))
    out_file = open('Training-Data/image_data/labels/%s.txt' % rawname, 'w')

    tree = ET.parse(in_file)
    root = tree.getroot()
    size = root.find('size')
    w = int(size.find('width').text)
    h = int(size.find('height').text)

    for obj in root.iter('object'):
        #difficult = obj.find('difficult').text
        cls = obj.find('name').text

        #if cls not in classes or int(difficult) == 1:
        if cls not in classes or int(difficult) == 1:
            continue

        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text),
             float(xmlbox.find('ymin').text), float(xmlbox.find('ymax').text))
        bb = convert((w, h), b)
        out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')

    out_file.close()
    in_file.close()


def txt2xml(wd, main_file, rawname, classes, image_id):
    if not ('%s/%s.txt' % (main_file, rawname) == 'Training-Data/image_data/labels/%s.txt' % rawname):
        subprocess.check_output(['cp', '%s/%s.txt' % (main_file, rawname), 'Training-Data/image_data/labels'])
    in_file = open('%s/%s.txt' % (main_file, rawname))
    readIn = []
    for line in in_file:
        readIn.append(line.strip('\n').split(' '))
    out_file = open('Training-Data/image_data/annotations/%s.xml' % rawname, 'w')
    out_file.write("<annotation>\n")
    out_file.write("\t<folder>images</folder>\n")
    out_file.write("\t<filename>%s</filename>\n" % image_id)
    out_file.write("\t<path>%s/Training-Data/image_data/images/%s</path>\n" % (wd, image_id))
    out_file.write("\t<source>\n\t\t<database>Unknown</database>\n\t</source>\n")
    im = Image.open('Training-Data/image_data/images/%s' % image_id)
    width, height = im.size
    out_file.write(
        "\t<size>\n\t\t<width>%d</width>\n\t\t<height>%d</height>\n\t\t<depth>3</depth>\n\t</size>\n" %
        (width, height))
    out_file.write("\t<segmented>0</segmented>\n")
    for datapoint in readIn:
        name = classes[int(datapoint[0])]
        centerx = int(float(datapoint[1]) * width)
        centery = int(float(datapoint[2]) * height)
        boxwidth = int(float(datapoint[3]) * width)
        boxheight = int(float(datapoint[4]) * height)
        out_file.write("\t<object>\n")
        out_file.write("\t\t<name>%s</name>\n\t\t<pose>Unspecified</pose>\n\t\t<truncated>0</truncated>\n" % name)
        out_file.write("\t\t<occluded>0</occluded>\n\t\t<difficult>0</difficult>\n\t\t<bndbox>\n")
        out_file.write("\t\t\t<xmin>%d</xmin>\n\t\t\t<ymin>%d</ymin>\n" %
                       (centerx - int(boxwidth / 2), (centery - int(boxheight / 2))))
        out_file.write("\t\t\t<xmax>%d</xmax>\n\t\t\t<ymax>%d</ymax>\n" %
                       (centerx + int(boxwidth / 2), (centery + int(boxheight / 2))))
        out_file.write("\t\t</bndbox>\n")
        out_file.write("\t</object>\n")
    out_file.write("</annotation>")
    out_file.close()
    in_file.close()


def traverseImages(main_file, classes):
    wd = getcwd()
    list_file = open('Training-Data/imagelist.txt', 'w')
    list_file_test = open('Training-Data/imagetestlist.txt', 'w')
    testcounter = 0
    extensions = ('.jpg', '.png')
    for image_id in os.listdir(main_file):
        testcounter += 1
        ext = os.path.splitext(image_id)[-1].lower()
        if ext not in extensions:
            continue
        print("Processing %s ..." % image_id)
        subprocess.check_output(['cp', '%s/%s' % (main_file, image_id), 'Training-Data/image_data/images'])
        rawname = os.path.splitext(image_id)[0]
        if os.path.isfile('%s/%s.txt' % (main_file, rawname)):
            if (testcounter % 10 != 0):
                list_file.write('%s/Training-Data/image_data/images/%s\n' % (wd, image_id))
            else:
                list_file_test.write('%s/Training-Data/image_data/images/%s\n' % (wd, image_id))
            txt2xml(wd, main_file, rawname, classes, image_id)
        elif os.path.isfile('%s/%s.xml' % (main_file, rawname)):
            if (testcounter % 10 != 0):
                list_file.write('%s/Training-Data/image_data/images/%s\n' % (wd, image_id))
            else:
                list_file_test.write('%s/Training-Data/image_data/images/%s\n' % (wd, image_id))
            xml2txt(main_file, rawname, classes)
        else:
            subprocess.check_output(['mv', 'Training-Data/image_data/images/%s' %
                                     image_id, 'Training-Data/image_data/unlabeled'])
    list_file.close()
    list_file_test.close()


def customizedTraverseImages(main_file, label_file, classes):
    wd = getcwd()
    testcounter = 0
    list_file = open('Training-Data/imagelist.txt', 'a')
    list_file_test = open('Training-Data/imagetestlist.txt', 'a')
    labels = open(label_file)
    for line in labels:
        temp = line.strip('\n').split(' ')
        while (len(temp) > 6):
            temp.pop()
        if len(temp) < 5:
            continue
        imgname = temp.pop(0)
        if os.path.isfile('%s/%s' % (main_file, imgname)):
            if not os.path.isfile('Training-Data/image_data/images/%s' % imgname):
                testcounter += 1
                if (testcounter % 10 == 0):
                    list_file_test.write('%s/Training-Data/image_data/images/%s\n' % (wd, imgname))
                else:

                    list_file.write('%s/Training-Data/image_data/images/%s\n' % (wd, imgname))
                subprocess.check_output(['cp', '%s/%s' % (main_file, imgname), 'Training-Data/image_data/images'])
            try:
                im = Image.open('Training-Data/image_data/images/%s' % imgname)
            except:
                continue

            width, height = im.size
            rawname = os.path.splitext(imgname)[0]
            if os.path.isfile('Training-Data/image_data/labels/%s.txt' % rawname):
                imglabel_file = open('Training-Data/image_data/labels/%s.txt' % rawname, 'a')
                imglabel_file.write('\n')
            else:
                imglabel_file = open('Training-Data/image_data/labels/%s.txt' % rawname, 'w')
            temp_corrected = [
                temp[0],
                str(int(temp[1]) / float(width) + (int(temp[3]) / float(width) / 2)),
                str(int(temp[2]) / float(height) + (int(temp[4]) / float(height) / 2)),
                str(int(temp[3]) / float(width)),
                str(int(temp[4]) / float(height))]
            imglabel_file.write(" ".join(temp_corrected))
            imglabel_file.close()
            txt2xml(wd, 'Training-Data/image_data/labels', rawname, classes, imgname)

    list_file.close()
    list_file_test.close()

def customizedTraverseImages_scale(main_file, label_file, classes, targetSize):
    wd = getcwd()
    testcounter = 0
    list_file = open('Training-Data/imagelist.txt', 'a')
    list_file_test = open('Training-Data/imagetestlist.txt', 'a')
    labels = open(label_file)
    for line in labels:
        temp = line.strip('\n').split(' ')
        while (len(temp) > 6):
            temp.pop()
        if len(temp) < 5:
            continue
        imgname = temp.pop(0)
        if os.path.isfile('%s/%s' % (main_file, imgname)):
            try:
                im = Image.open('%s/%s' % (main_file, imgname))
            except:
                continue

            if not os.path.isfile('Training-Data/image_data/images/%s' % imgname):
                testcounter += 1
                if (testcounter % 10 == 0):
                    list_file_test.write('%s/Training-Data/image_data/images/%s\n' % (wd, imgname))
                else:

                    list_file.write('%s/Training-Data/image_data/images/%s\n' % (wd, imgname))
                #subprocess.check_output(['cp', '%s/%s' % (main_file, imgname), 'Training-Data/image_data/images'])
                try:
                    im.thumbnail(targetSize, Image.ANTIALIAS)
                except:
                    continue
                im.save('Training-Data/image_data/images/%s' % imgname, "JPEG")

            width, height = im.size
            rawname = os.path.splitext(imgname)[0]
            if os.path.isfile('Training-Data/image_data/labels/%s.txt' % rawname):
                imglabel_file = open('Training-Data/image_data/labels/%s.txt' % rawname, 'a')
                imglabel_file.write('\n')
            else:
                imglabel_file = open('Training-Data/image_data/labels/%s.txt' % rawname, 'w')
            temp_corrected = [
                temp[0],
                str(float(temp[1]) / float(width) + float(temp[3]) / float(width) / 2),
                str(float(temp[2]) / float(height) + float(temp[4]) / float(height) / 2),
                str(float(temp[3]) / float(width)),
                str(float(temp[4]) / float(height))]
            imglabel_file.write(" ".join(temp_corrected))
            imglabel_file.close()
            txt2xml(wd, 'Training-Data/image_data/labels', rawname, classes, imgname)

    list_file.close()
    list_file_test.close()


def main():
    if (len(sys.argv) == 1):
        assert(0), "Please specify main data folder in arguments."
    elif (len(sys.argv) == 2):
        main_file = sys.argv[1]
        classes = getClasses(main_file)
        setUp()
        traverseImages(main_file, classes)
    elif (len(sys.argv) == 3):  # first argument is the folder with images, and second argument is list of labels
        main_file = sys.argv[1]
        label_file = sys.argv[2]
        classes = getClasses(main_file)
        setUp()
        customizedTraverseImages(main_file, label_file, classes)
    else:
        assert(0), "Incorrect number of arguments. See README for details."


def convertMidea60gb():
    classes = ['head', 'hand']
    annotation_folder = '/media/shawn/DataA/backup/midea60gb/annotationFiles/mideaLab/LabA'
    image_folder_parent = '/media/shawn/DataA/backup/midea60gb/mideaLab/LabA'
    setUp()
    for label_file in glob(os.path.join(annotation_folder, '*.txt')):
        items = label_file.split('/')
        label_file_name = items[-1]
        items = label_file_name.split('.')
        image_directory_name = items[0]
        image_directory = os.path.join(image_folder_parent, image_directory_name)
        #customizedTraverseImages(image_directory, label_file, classes)
        customizedTraverseImages_scale(image_directory, label_file, classes, (224, 224))



if __name__ == '__main__':
    main()
    # convertMidea60gb()
